package com.jonbott.rxjavasimplified.util

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposables

//Mark: - Traits
class TraitsRx {

    companion object {
        var shared = TraitsRx()
    }

    private var bag = CompositeDisposable()

    fun traits_single() {
        val single = Single.create<String> { emitter ->
            //do some logic here
            val success = true


            if (success)
                emitter.onSuccess("nice work!")   //return a value
            else
                emitter.onError(RuntimeException("Some faked exception"))

            emitter.setDisposable(Disposables.fromAction {
                //Clean up resources
            })
        }

        single.subscribe({ result -> //onSuccess
            //do something with result
            print("single: $result")
        }, { error -> //onError
            //do something for error
        }).disposedBy(bag)
    }

    fun traits_completable() {
        val completable = Completable.create { emitter ->
            //do some logic here
            val success = true


            if (success)
                emitter.onComplete()
            else
                emitter.onError(RuntimeException("Some faked exception"))

            emitter.setDisposable(Disposables.fromAction {
                //Clean up resources
            })
        }


        completable.subscribe({ //onCompleted
            //handle on complete
            print("Completable completed")
        }, { error ->           //onError
            //do something for error
        }).disposedBy(bag)
    }

    fun traits_maybe() {
        val maybe = Maybe.create<String> { emitter ->
            //do some logic here
            val success = true
            val hasResult = true

            if (success) {

                if (hasResult) {
                    emitter.onSuccess("some result")
                } else {
                    emitter.onComplete() //no result
                }

            } else {
                emitter.onError(RuntimeException("Some faked exception"))

                emitter.setDisposable(Disposables.fromAction {
                    //Clean up resources
                })
            }
        }

        maybe.subscribe({ result ->     //onSuccess
            //do something with result
            print("Maybe - result: $result")
        }, { error ->                   //onError
            //do something for error
        }, { //complete                 //onComplete
            //handle on complete
            print("Completable completed")
        })
    }

}