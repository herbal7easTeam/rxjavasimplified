package com.jonbott.rxjavasimplified.util

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


val Int.isEven: Boolean
    get() = this % 2 == 0


val Int.isOdd: Boolean
    get() = !this.isEven


fun Disposable.disposedBy(bag: CompositeDisposable) = bag.add(this)
