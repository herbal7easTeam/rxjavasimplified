package com.jonbott.rxjavasimplified.util

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject

class RxVariable<T>(initialValue: T) {

    private val serializedSubject: Subject<T> = BehaviorSubject.createDefault(initialValue).toSerialized()

    var _value = initialValue

    var value: T
        @Synchronized get() = _value
        @Synchronized set(newValue) {
            _value = newValue
            serializedSubject.onNext(newValue)
        }

    fun asObservable(): Observable<T> {
        return serializedSubject
    }
}



class RxOptionalVariable<T>(initialValue: T) {

    private val serializedSubject: Subject<OptionalBox<T>> = BehaviorSubject.createDefault(initialValue.asOptional()).toSerialized()

    var _value = initialValue.asOptional()

    var value: T?
        @Synchronized get() = _value.value
        @Synchronized set(newValue) {
            _value = newValue.asOptional()
            serializedSubject.onNext(_value)
        }

    fun asObservable(): Observable<OptionalBox<T>> {
        return serializedSubject
    }
}

