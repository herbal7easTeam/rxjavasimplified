package com.jonbott.rxjavasimplified.util

import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article

typealias ArticleLambda = (Article?)->Unit
typealias VoidLambda    = ()->Unit

enum class NetworkError {
    LoadArticleFailed
}