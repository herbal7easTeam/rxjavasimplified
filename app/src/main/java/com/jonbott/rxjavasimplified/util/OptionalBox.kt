package com.jonbott.rxjavasimplified.util

data class OptionalBox<T>(val value: T?)

fun <T> T?.asOptional() = OptionalBox(this)
