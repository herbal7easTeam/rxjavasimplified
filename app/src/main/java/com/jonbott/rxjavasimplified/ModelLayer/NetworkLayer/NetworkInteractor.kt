package com.jonbott.rxjavasimplified.ModelLayer.NetworkLayer

import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Person
import com.jonbott.rxjavasimplified.ModelLayer.NetworkLayer.EndpointInterfaces.JsonPlaceHolder
import com.jonbott.rxjavasimplified.util.ArticleLambda
import com.jonbott.rxjavasimplified.util.NetworkError
import com.jonbott.rxjavasimplified.util.RxOptionalVariable
import com.jonbott.rxjavasimplified.util.VoidLambda
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class NetworkInteractor() {

    //region Observables

    val currentArticle = RxOptionalVariable<Article?>(null)
    val errorState     = RxOptionalVariable<NetworkError?>(null)

    //endregion

    //region Fields

    private val placeHolderApi: JsonPlaceHolder = JsonPlaceHolder.Factory.create()
    private val fakeNetwork = FakeNetwork()

    //endregion


    //region #1 - Success & Failure Blocks

    fun getArticle(articleId: String, success:ArticleLambda, failure: VoidLambda) {
        val call = placeHolderApi.getArticle(articleId)

        call.enqueue(object: Callback<Article> {
            override fun onResponse(call: Call<Article>?, response: Response<Article>?) {
                val article = parseRespone(response)
                success(article)
            }

            override fun onFailure(call: Call<Article>?, t: Throwable?) {
                println("Failed to GET Article: ${ t?.message }")
                failure()
            }
        })
    }

    //endregion

    //region #2 - Set Local Variable

    fun getArticle(articleId: String) {
        val call = placeHolderApi.getArticle(articleId)

        call.enqueue(object : Callback<Article> {
            override fun onResponse(call: Call<Article>?, response: Response<Article>?) {
                val article = parseRespone(response)
                currentArticle.value = article
            }

            override fun onFailure(call: Call<Article>?, t: Throwable?) {
                println("Failed to GET Article: ${t?.message}")
                errorState.value = NetworkError.LoadArticleFailed
            }
        })
    }

    //endregion

    //region #3 - Bubble up Endpoint Result as RxSingle

    fun getArticleRx(articleId: String): Single<Article> {
        return placeHolderApi.getArticleRx(articleId)
    }

    //endregion

    //region #4 - Grouping Tasks

    private fun buildGetInfoNetworkCallForPerson(person: Person): Maybe<String> {
        return Maybe.create { emitter ->  //emitter is subscriber

            fakeNetwork.getPersonInfo(person) { result ->

                result.fold({ optional ->

                    if (optional.value != null) {
                        emitter.onSuccess(optional.value)
                    } else {
                        emitter.onComplete()
                    }

                }, { error ->
                    emitter.onError(error)
                })
            }
        }

    }

    //Make observables for an array of people
    fun loadInfoFor(people: List<Person>): Single<MutableList<String>> {

        //Foreach person make network call
        val networkObservables = people.map(::buildGetInfoNetworkCallForPerson)

        // when all server results have returned merge observables into a single observable
        return Maybe.merge(networkObservables)
                .toList()
    }

    //endregion

    //region Helper Methods

    private fun parseRespone(response: Response<Article>?): Article? {
        val article = response?.body()

        if (article == null) {
            parseResponeError(response)
        }

        return article
    }

    private fun parseResponeError(response: Response<Article>?) {
        if(response == null) return

        val responseBody = response.errorBody()

        if(responseBody != null) {
            try {
                println("❗ ️responseBody = ${ responseBody.string() }")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else {
            println("❗️ responseBody == null")
        }
    }

    //endregion
}






/* ignore

    //Wrap task in observable
    //This pattern is used often for units of work
//    private fun buildGetInfoNetworkCallForPerson(person: Person): Observable<OptionalBox<String?>> {
//        return Observable.create { observer ->
//            //Execute Request - Do actual work here
//            getPersonInfo(person) { result ->
//                result.fold({ optional ->
//                    observer.onNext(optional) //push result
//                    observer.onComplete() //finish single unit of work
//                }, { error ->
//                    observer.onError(error)
//                })
//            }
//
//            observer.setDisposable(Disposables.fromAction {
//                println("Custom Disposable used")  //cleanup dependencies - in this case we have none
//            })
//        }
//    }


//        return Observable.create { observer ->
//            //Execute Request - Do actual work here
//            getPersonInfo(person) { result ->
//                result.fold({ optional ->
//                    observer.onNext(optional) //push result
//                    observer.onComplete() //finish single unit of work
//                }, { error ->
//                    observer.onError(error)
//                })
//            }
//
//            observer.setDisposable(Disposables.fromAction {
//                println("Custom Disposable used")  //cleanup dependencies - in this case we have none
//            })
//        }



    fun postArticle(article: Article, success: ArticleLambda, failure: VoidLambda) {
        val call = placeHolderApi.postArticle(article)

        call.enqueue(object: Callback<Article>{
            override fun onResponse(call: Call<Article>?, response: Response<Article>?) {
                val article = parseRespone(response)
                success(article)
            }

            override fun onFailure(call: Call<Article>?, t: Throwable?) {
                println("Failed to POST Article: ${ t?.message }")
                failure()
            }
        })
    }


 */