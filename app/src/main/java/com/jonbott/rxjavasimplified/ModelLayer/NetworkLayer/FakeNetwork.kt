package com.jonbott.rxjavasimplified.ModelLayer.NetworkLayer

import com.github.kittinunf.result.Result
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Person
import com.jonbott.rxjavasimplified.util.OptionalBox
import com.jonbott.rxjavasimplified.util.asOptional
import com.jonbott.rxjavasimplified.util.isEven
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.io.IOException

typealias StringResultLambda = (Result<OptionalBox<String?>, Exception>) -> Unit

class FakeNetwork() {
    //Create a Task
    fun getPersonInfo(person: Person, finished: StringResultLambda) {

        //Execute on Background thread
        //do your task here
        launch {

            println("🚦 start network call: $person")

            val multiplier = person.age + 1
            val randomTimeMilliSeconds = multiplier * 1_000

            delay(randomTimeMilliSeconds) //fake some work

            println("🏁 finished network call: $person")

            val description = person.description.asOptional<String?>()

            var result = Result.of(description)

            //Example 2
            result = if(person.age == 4) {
                Result.error(IOException("This is a fake network error"))
            } else {
                Result.of(description)
            }

            finished(result)
        }
    }
}


private val Person.description: String? //just to get some null values to filter
    get(){
        return if (age.isEven)
            toString()
        else
            null
    }