package com.jonbott.rxjavasimplified.ModelLayer.Entities

data class Article(
        var id: Int,
        var userId: Int,
        var title: String,
        var body: String
)

data class Person (
    var firstName: String,
    var lastName: String,
    var age: Int
)