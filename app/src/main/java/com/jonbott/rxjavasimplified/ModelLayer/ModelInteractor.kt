package com.jonbott.rxjavasimplified.ModelLayer

import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Person
import com.jonbott.rxjavasimplified.ModelLayer.NetworkLayer.NetworkInteractor
import com.jonbott.rxjavasimplified.util.NetworkError
import com.jonbott.rxjavasimplified.util.RxOptionalVariable
import io.reactivex.Single

class ModelInteractor() {

    val currentArticle: RxOptionalVariable<Article?>
        get() = networkInteractor.currentArticle

    val errorState: RxOptionalVariable<NetworkError?>
        get() = networkInteractor.errorState

    private val networkInteractor = NetworkInteractor()

    fun getArticle(articleId: String) {
        networkInteractor.getArticle(articleId)
    }

    fun getArticleRx(articleId: String): Single<Article> {
        return networkInteractor.getArticleRx(articleId)
    }

    fun loadInfoFor(people: List<Person>): Single<MutableList<String>> {
        return networkInteractor.loadInfoFor(people)
    }
}