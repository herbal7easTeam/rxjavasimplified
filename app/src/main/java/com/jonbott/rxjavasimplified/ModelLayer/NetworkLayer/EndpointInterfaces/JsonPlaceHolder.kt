package com.jonbott.rxjavasimplified.ModelLayer.NetworkLayer.EndpointInterfaces

import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article
import io.reactivex.Single
import retrofit2.Call
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path


interface JsonPlaceHolder {

    @GET("/posts/{articleId}")
    fun getArticle(@Path("articleId") articleId: String): Call<Article>

    @GET("/posts/{articleId}")
    fun getArticleRx(@Path("articleId") articleId: String): Single<Article>


    companion object Factory {

        val API_BASE_URL = "https://jsonplaceholder.typicode.com" //http://localhost:3000";

        fun create(): JsonPlaceHolder {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(API_BASE_URL)
                    .build()

            return retrofit.create(JsonPlaceHolder::class.java)
        }
    }
}