package com.jonbott.rxjavasimplified.Activites

import com.jakewharton.rxrelay2.BehaviorRelay
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Person
import com.jonbott.rxjavasimplified.ModelLayer.ModelInteractor
import com.jonbott.rxjavasimplified.util.*
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

class MainPresenter {


    //region Lifecycle - Clean Up

    val bag = CompositeDisposable()

    //endregion

    //region Fields

    val defaultErrorMessage = "Unable to load the article"

    private var currentArticleId = 0
    private val modelInteractor = ModelInteractor()

    var people = listOf(Person(firstName = "Norris",   lastName = "Najar",     age = 0 ),
                        Person(firstName = "Dylan",    lastName = "Decarlo",   age = 1 ),
                        Person(firstName = "Sonny",    lastName = "Stecher",   age = 2 ),
                        Person(firstName = "Napoleon", lastName = "Nicols",    age = 3 ),
                        Person(firstName = "Jinny",    lastName = "Jordahl",   age = 4 ),
                        Person(firstName = "Wendi",    lastName = "Woodhouse", age = 5 ))



    private val nextId: String
        get() {
            if(currentArticleId > 50) currentArticleId = 0 //reset

            currentArticleId++

            updateTitle(currentArticleId) //side effect

            return if(currentArticleId.isOdd)
                currentArticleId.toString()
            else
                "bad value"
//            return currentArticleId.toString()
        }

    //endregion

    //region LifeCycle

    init {
        setupObservables()
    }

    //endregion





    //region Observables

    //region Basics

    val title = RxVariable("Some Default Value")
    val title2 = BehaviorRelay.createDefault("Another Default Value")


    //endregion

    //region Optionals

    val errorMessage = RxOptionalVariable<String?>(null)
    val currentArticle: RxOptionalVariable<Article?>
        get() = modelInteractor.currentArticle

    //endregion

    //region Subjects

    val nextArticle: PublishSubject<Article> = PublishSubject.create()

    //endregion

    //region Subscriptions

    private fun setupObservables() {
        modelInteractor.errorState.asObservable().subscribe {
            val errorState = it.value ?: return@subscribe

            val message = messageFrom(errorState)
            errorMessage.value = message
        }.disposedBy(bag)
    }

    //endregion

    //endregion

    //region Consumed In Nextwork Layer

    fun getArticle() {
        modelInteractor.getArticle(nextId)
    }

    //endregion

    //region Consumed in View

    fun getArticleRx(): Single<Article> {
        return modelInteractor.getArticleRx(nextId)
    }

    fun loadPeopleInfo(): Single<MutableList<String>> {
        return modelInteractor.loadInfoFor(people)
    }

    //endregion

    //region Consumed In Presenter

    fun getArticleRx2() {
            modelInteractor.getArticleRx(nextId)
                    .subscribeOn(Schedulers.io())
                    .subscribe({ nextArticle.onNext(it) }, //onNext
                               { it.printStackTrace() }) //onError
                    .disposedBy(bag)

    }

    //endregion

    //region Helper Methods

    private fun messageFrom(errorState: NetworkError): String {
        return when(errorState) {
            NetworkError.LoadArticleFailed -> defaultErrorMessage
            else -> "An Unknown Error has occurred"
        }
    }

    private fun updateTitle(articleId: Int) {
        title.value = "Article $articleId"
    }


    //endregion
}

