package com.jonbott.rxjavasimplified.Activites

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jonbott.rxjavasimplified.ModelLayer.Entities.Article
import com.jonbott.rxjavasimplified.R
import com.jonbott.rxjavasimplified.util.OptionalBox
import com.jonbott.rxjavasimplified.util.RxVariable
import com.jonbott.rxjavasimplified.util.disposedBy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity() : AppCompatActivity() {

    //region Fields

    private val presenter = MainPresenter()
    private val bag = CompositeDisposable()

    //endregion

    //region Life Cycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setup()
    }

    override fun onDestroy() {
        bag.dispose()
        super.onDestroy()
    }

    //endregion

    //region Setup

    private fun setup() {

        loadButton.setOnClickListener {
            loadButtonClicked()
        }

        simplestExample()

        setupReactive_Variable()
        setupReactive_Subject()
        setupReactive_Task()
    }

    //endregion

    //region Basics

    private fun simplestExample() {
        //RxVariable
//        infoTextView.text = presenter.title.value
//
//        presenter.title.asObservable().subscribe { value ->
//            infoTextView.text = value
//        }.disposedBy(bag)

        infoTextView.text = presenter.title2.value

        presenter.title2.subscribe { value ->
            infoTextView.text = value
        }.disposedBy(bag)
    }



    private fun basicExample() {

        //region RxVariable

        //initial value
        val title = RxVariable("some default value")

        //change value
        title.value = "title has changed"

        //use value
        infoTextView.text = title.value

        //subscribe to changes
        title.asObservable().subscribe { value ->
            infoTextView.text = value
        }.disposedBy(bag)

        //endregion

        //region BehaviorRelay

        //initial value
        val title2 = BehaviorRelay.createDefault("Another default value")

        //change value
        title2.accept("title has changed again")

        //use value
        infoTextView.text = title2.value

        //subscribe to changes
        title2.subscribe { value ->
            infoTextView.text = value
        }.disposedBy(bag)

        //endregion
    }



    //endregion


    //region Rx Subscriptions

    //region #1 - Setup for Variable

    private fun setupReactive_Variable() {

//Three different ways

        val disposable = presenter.currentArticle.asObservable()
                        .skip(1)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe{ optionalBox ->
                            val article = optionalBox.value

                            if (article != null) {
                                present(article)
                            } else { //usually because of bad url, but not an error
                                Toast.makeText(this, presenter.defaultErrorMessage, Toast.LENGTH_SHORT).show()
                            }
                        }
        bag.add(disposable)



/*
        bag.add(
                presenter.currentArticle.asObservable()
                        .skip(1)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .subscribe(::presentArticle)
        )
*/

/*
        presenter.currentArticle.asObservable()
                .skip(1)
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe (::presentArticle)
                .disposedBy(bag)
*/

        presenter.errorMessage.asObservable()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    val errorMessage = it.value ?: return@subscribe

                    Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
                }.disposedBy(bag)
    }

    //endregion

    //region #2 - Setup for Subjects

    private fun setupReactive_Subject() {
        presenter.nextArticle
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { article ->
                    present(article)
                    println("nextArticle: $article")
                }.disposedBy(bag)
    }

    //endregion

    //region #3 - Setup for Tasks

    private fun setupReactive_Task() {
        presenter.loadPeopleInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ infoList ->
                    println("🦄 all processes completed successfully")
                    println("🦄 network results:")
                    println(infoList)
                }, { error ->
                    println("❗ ️not all processes completed successfully")
                    println("❗ ️error: ${error.localizedMessage}")
                }).disposedBy(bag)
    }

    //endregion

    //endregion

    //region Events

    private fun loadButtonClicked() {
        getArticle()
//        getArticleRx()
//        getArticleRx2()
    }

    private fun getArticle() {
        presenter.getArticle()
    }

    private fun getArticleRx() {  //New Subscription created each time, as opposed to methods in the "Rx Subscriptions" region

        presenter.getArticleRx()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())  //Be careful with threads
                .subscribe(::present, //onNext
                           { it.printStackTrace() }) //onError
                .disposedBy(bag)
    }

    private fun getArticleRx2() {
        presenter.getArticleRx2()
    }

    //endregion

    //region Helper Methods

    private fun presentArticle(optional: OptionalBox<Article?>) {
        val article = optional.value

        if (article != null) {
            present(article)
        } else { //usually because of bad url, but not an error
            Toast.makeText(this, presenter.defaultErrorMessage, Toast.LENGTH_SHORT).show()
        }
    }

    private fun present(article: Article) {
        titleTextView.text = article.title
        bodyTextView.text = article.body
    }

    //endregion
}
