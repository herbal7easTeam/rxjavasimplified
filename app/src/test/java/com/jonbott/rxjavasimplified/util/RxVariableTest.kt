package com.jonbott.rxjavasimplified.util

import org.junit.After
import org.junit.Before
import org.junit.Test

class RxVariableTest {
    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun valueIsAsExpected() {
        val expected = "some initial value"

        val rxVariable = RxVariable(expected)

        kotlin.test.assertEquals(expected, rxVariable.value)
    }

    @Test
    fun valueChangesAsExpected() {
        val expected = "new value"
        val rxVariable = RxVariable("some initial value")

        rxVariable.value = expected

        kotlin.test.assertEquals(expected, rxVariable.value)
    }

    @Test
    fun initialValueIsAsExpected() {
        val expected = "some initial value"
        val rxVariable = RxVariable(expected)

        rxVariable.asObservable().subscribe({
            kotlin.test.assertEquals(expected, it, "values don't match")
        })
    }

    @Test
    fun initialValueIsAsExpectedAfterChange() {
        val expected = "new value"
        val rxVariable = RxVariable("some initial value")

        //skipping initial value
        rxVariable.asObservable().skip(1).subscribe({
            kotlin.test.assertEquals(expected, it, "values don't match")
        })

        rxVariable.value = expected
    }

//region unused tests

//    @Test
//    fun initialValueIsAsExpected() {
//        val expected = "some initial value"
//        val rxVariable = RxVariable(expected)
////        val observer = TestObserver.create<String>()
//
//
//        rxVariable.asObservable().subscribe({
//            assertEquals(expected, it, "values don't match")
//        })
//    }

//        rxVariable.asObservable().subscribe(observer)
//
//        observer.assertNotComplete()
//        observer.assertNoErrors()
//        observer.assertValueCount(1)
//        assertTrue { observer.events.count() == 1 }
//        assertThat(observer.events[0],
//                hasItems(expected))
//    }

//endregion

}

