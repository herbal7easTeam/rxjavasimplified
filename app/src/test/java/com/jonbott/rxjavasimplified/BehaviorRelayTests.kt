package com.jonbott.rxjavasimplified

import com.jakewharton.rxrelay2.BehaviorRelay
import org.junit.Test
import kotlin.test.assertEquals

class BehaviorRelayTests {

    @Test
    fun valueIsAsExpected() {
        val expected = "some initial value"

        val relay = BehaviorRelay.createDefault(expected)

        assertEquals(expected, relay.value)
    }

    @Test
    fun valueChangesAsExpected() {
        val expected = "new value"
        val relay = BehaviorRelay.createDefault("some initial value")

        relay.accept(expected)

        assertEquals(expected, relay.value)
    }

    @Test
    fun initialValueIsAsExpected() {
        val expected = "some initial value"
        val rxVariable = BehaviorRelay.createDefault(expected)

        rxVariable.subscribe({
            assertEquals(expected, it, "values don't match")
        })
    }

    @Test
    fun initialValueIsAsExpectedAfterChange() {
        val expected = "new value"
        val rxVariable = BehaviorRelay.createDefault("some initial value")

        //skipping initial value
        rxVariable.skip(1).subscribe({
            assertEquals(expected, it, "values don't match")
        })

        rxVariable.accept(expected)
    }

//region unused tests

//    @Test
//    fun initialValueIsAsExpected() {
//        val expected = "some initial value"
//        val rxVariable = RxVariable(expected)
////        val observer = TestObserver.create<String>()
//
//
//        rxVariable.asObservable().subscribe({
//            assertEquals(expected, it, "values don't match")
//        })
//    }

//        rxVariable.asObservable().subscribe(observer)
//
//        observer.assertNotComplete()
//        observer.assertNoErrors()
//        observer.assertValueCount(1)
//        assertTrue { observer.events.count() == 1 }
//        assertThat(observer.events[0],
//                hasItems(expected))
//    }

//endregion

}
